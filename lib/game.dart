import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Game extends StatelessWidget {
  const Game({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final landscapeHeight = screenHeight < AppLayout.widthFirstBreakpoint + 50;

    return Scaffold(
      backgroundColor: Colors.red,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            titleWidget(context, landscapeHeight),
          ],
        ),
      ),
    );
  }
}

class AppLayout {
  static const double widthFirstBreakpoint = 450.0;
}

Widget titleWidget(BuildContext context, bool landscapeHeight) {
  return Padding(
      padding: const EdgeInsets.only(top: 26.0, bottom: 10.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios_new,
                  color: Colors.white,
                )),
          ),
          Expanded(
              flex: 5,
              child: Text(
                "Kids Math",
                style: GoogleFonts.bungee(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
                textScaleFactor: landscapeHeight ? 2.7 : 2.2,
                textAlign: TextAlign.center,
              )),
          const Expanded(flex: 1, child: Text(""))
        ],
      ));
}
