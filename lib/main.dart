import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'home.dart';

void main() {
  runApp(const MathGameApp());
}

class MathGameApp extends StatelessWidget {
  const MathGameApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Kids Math',
        theme: ThemeData(
          primarySwatch: Colors.deepPurple,
        ),
        home: LayoutBuilder(
          builder: (context, constraints) {
            final double scalePortrait = constraints.maxWidth/390;
            final double scaleLandscape = constraints.maxHeight/390;
            return constraints.maxWidth < constraints.maxHeight ? HomePage(scale: scalePortrait): HomePage(scale: scaleLandscape);
          },
        ),
      ),
    );
  }
}
