import 'dart:io';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'game.dart';
import 'gameplay.dart';

class HomePage extends StatelessWidget {
  final double scale;

  const HomePage({Key? key, required this.scale}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    String imgPortrait = "space_p.gif";
    String imgLandscape = "space_l.gif";
    return Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image:
              isPortrait ? AssetImage(imgPortrait) : AssetImage(imgLandscape),
              fit: BoxFit.cover),
              ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          const Spacer(flex: 4),
          Flexible(
            child: Text(
              "Kids Math",
              style: GoogleFonts.bungee(
                fontSize: isPortrait ? 55 * scale : 55 * scale,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
            flex: 3,
          ),
          OutlinedButton(
              style: OutlinedButton.styleFrom(
                fixedSize: Size(170, 75),
                backgroundColor: Colors.red[600],
                side: BorderSide(width: 5, color: Colors.white),
              ),
              //ไปหน้าที่มีappbar
              // onPressed: () {
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (context) => const Game()));
              // },
              //ไปหน้าที่ไม่มีappbar
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const game()));
              },
              child: Text(
                "PLAY",
                style: GoogleFonts.bungee(
                  fontSize: 30,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              )
            ),
          OutlinedButton(
              style: OutlinedButton.styleFrom(
                fixedSize: Size(170, 75),
                backgroundColor: Colors.green[600],
                side: BorderSide(width: 5, color: Colors.white),
              ),
              
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    titlePadding: EdgeInsets.all(0.0),
                    actions: [
                      TextButton(onPressed: () 
                      { Navigator.pop(context); },                       
                      child: Text('close',
                      style: GoogleFonts.bungee(
                          fontSize: 10,
                          color: Colors.black,      
                        ), 
                      ))                      
                    ],

                    title: Container(
                      padding: EdgeInsets.all(10.0),
                      color: Colors.yellow[800],
                      child: Text(
                        "HOW TO PLAY",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.bungee(
                          fontSize: 25,
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    contentPadding: const EdgeInsets.all(20),
                    
                    content: Text(
                      "Click Play to start the game, Answer the questions within the time limit.",
                      textAlign: TextAlign.center,
                        style: GoogleFonts.bungee(
                          fontSize: 15,
                          fontWeight: FontWeight.w200,
                          color: Colors.black,
                      ),
                    ),
                  ),
                );
              },

              child: Text(
                "INFO",
                style: GoogleFonts.bungee(
                  fontSize: 30,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              )),
              const Spacer(flex: 4),
        ],
      ),
    ));
  }
}
