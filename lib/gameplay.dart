import 'dart:async';
import 'dart:math';

import 'package:example_template/home.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'home.dart';

class game extends StatefulWidget{
  const game({Key? key}) : super(key: key);

  @override
  State<game> createState() => _game();
}

// var SetTextStyle = const TextStyle(fontWeight: FontWeight.bold,fontSize: 32,color: Colors.white);

class _game extends State<game>{
//NumPad
  List<String> NumPad = [
    '1',
    '2',
    '3',
    '4',
  ];
  int signNum = 0;
  List<String> sign = [
    '+',
    '+',
    '-',
    '*',
    '*',
  ];
  int num1 = 1;
  int num2 = 2;
  int score = 0;
  int result = 0;

  void init(){
    if(signNum == 0){
      int ans = num1+num2;
      NumPad[0] = "4";
      NumPad[1] = ans.toString();
      NumPad[2] = "6";
      NumPad[3] = "2";
    }else if(signNum == 1){
      int ans = num1+num2;
      NumPad[0] = "9";
      NumPad[1] = "12";
      NumPad[2] = "7";
      NumPad[3] = ans.toString();
    }else if(signNum == 2){
      int ans = num1-num2;
      NumPad[0] = ans.toString();
      NumPad[1] = "3";
      NumPad[2] = "8";
      NumPad[3] = "1";
    }else if(signNum == 3){
      int ans = num1*num2;
      NumPad[0] = "25";
      NumPad[1] = ans.toString();
      NumPad[2] = "12";
      NumPad[3] = "5";
    }else if(signNum == 4){
      int ans = num1*num2;
      NumPad[0] = ans.toString();
      NumPad[1] = "9";
      NumPad[2] = "4";
      NumPad[3] = "10";
    }
  }

  //input
  String input = '';

  //stage number
  int stage = 1;

  static const maxSecond = 10;
  int seconds = maxSecond;
  Timer? timer;
  void startTimer (){
      timer = Timer.periodic(Duration(seconds : 1), (_) { 
      setState(() => seconds--);
      checkTime();
    });
  }
  void checkTime(){
    if(seconds <= 0){
      stopTime();
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text('Time is Out!',
              style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: restart,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.replay,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      }
      );
    }
  }
  void stopTime(){
    timer?.cancel();
  }

  //buttonPressFunction
  void buttonPressFunction(String button){
    setState(() {
        input += button;
        CheckAns();
    });
  }
  
  void CheckAns(){
    stopTime();
    if(signNum == 0){
      if(num1+num2 == int.parse(input)){
      score++;
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text('Correct!',
              style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: Continued,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.arrow_circle_right,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      });
    }else{
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text('Incorrect!',style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: restart,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.replay,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      }
      );
    }
    }else if(signNum == 1){
      if(num1+num2 == int.parse(input)){
      score++;
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text('Correct!',style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: Continued,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.arrow_circle_right,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      });
    }else{
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text('Incorrect!',style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: restart,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.replay,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      }
      );
    }
    }else if(signNum == 2){
      if(num1-num2 == int.parse(input)){
      score++;
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text('Correct!',style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: Continued,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.arrow_circle_right,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      });
    }else{
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text('Incorrect!',style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: restart,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.replay,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      }
      );
    }
    }else if(signNum == 3){
      if(num1*num2 == int.parse(input)){
      score++;
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text('Correct!',style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: Continued,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.arrow_circle_right,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      });
    }else{
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text('Incorrect!',style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: restart,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.replay,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      }
      );
    }
    }else if(signNum == 4){
      if(num1*num2 == int.parse(input)){
      score++;
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text('Congratulation!!',
              style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 21,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: restart,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.replay,color: Colors.white,)),
                      ),
              ),
            ),
            Center(
              child: GestureDetector(
                onTap: (){
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.home,color: Colors.white,)),
                      ),
              ),
            ),
            
            ],),),
        );
      });
    }else{
      showDialog(context: (context), builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Container(height: 200,
          color: Colors.indigo,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text('Incorrect!',style: GoogleFonts.bungee(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30,
                ),
              ),
            Center(
              child: GestureDetector(
                onTap: restart,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Center(child: Icon(Icons.replay,color: Colors.white,)),
                      ),
              ),
            )],),),
        );
      }
      );
    }
    }
    
  }

  var randomNum = Random();

  void Continued(){
    stage ++;
    signNum ++;
    Navigator.of(context).pop();
    seconds = maxSecond;
    startTimer();
    setState(() {
      input = '';
    });

    num1 = randomNum.nextInt(10);
    num2 = randomNum.nextInt(10);
    
  }
  
  void restart(){
   stage = 0;
   signNum = 0;
   int num1 = 1;
   int num2 = 2;
   Continued();
  }
  
  @override
  Widget build(BuildContext context){
    init();
    final screenHeight = MediaQuery.of(context).size.height;
    final landscapeHeight = screenHeight < AppLayout.widthFirstBreakpoint + 50;

    String img = "space_g.gif";

    return Scaffold(
      // Background
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image:AssetImage(img),
            fit: BoxFit.cover),
            ),
            
            child: Column(
              children: [
                Container(
                  child: Column(
                    children: <Widget>[
                      titleWidget(context, landscapeHeight),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 40),
            height: 50,
            // color:Color.fromARGB(255, 208, 13, 176),
            // child: Text('Stage: '+ stage.toString() + ' / 5',style: SetTextStyle,),
            
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.flag, color: Colors.white,),
                SizedBox(width: 10),
                Text(
                'Stage : '+ stage.toString() + ' / 5',
                style: GoogleFonts.bungee(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  ),
                  textScaleFactor: landscapeHeight ? 2.7 : 2.2,
                ),
              ]),

            // child: Text(
            //     'Stage : '+ stage.toString() + ' / 5',
            //     style: GoogleFonts.bungee(
            //       fontWeight: FontWeight.w500,
            //       color: Colors.white,
            //     ),
            //     textScaleFactor: landscapeHeight ? 2.7 : 2.2,
            //   ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            height: 50,
            // color:Color.fromARGB(255, 208, 13, 176),
            // child: Text('Time: '+ seconds.toString(),style: SetTextStyle,),

            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
              Icon(Icons.timer, color: Colors.white,),
              SizedBox(width: 15, height: 25,),
              Text(
                'Time : '+ seconds.toString(),
                style: GoogleFonts.bungee(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
                textScaleFactor: landscapeHeight ? 2.7 : 2.2,
              ),
            ]),
            // child: Text(
            //     'Time : '+ seconds.toString(),
            //     style: GoogleFonts.bungee(
            //       fontWeight: FontWeight.w500,
            //       color: Colors.white,
            //     ),
            //     textScaleFactor: landscapeHeight ? 2.7 : 2.2,
            //   ),
          ),

          //Quiz
          Expanded(
            child: Container(
              child: Center(
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(num1.toString() + ' ' + sign[signNum].toString()+ ' ' + num2.toString() + ' = ',
                      style: GoogleFonts.bungee(
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                        fontSize: 30,
                        ),
                      ),
                    //input
                    Container(
                      height: 50,
                      width: 100,
                      color: Color.fromARGB(255, 243, 187, 3),
                      child: Center(
                        child: Text(input, style: GoogleFonts.bungee(
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                          fontSize: 30,
                          ),
                        ),
                      ), 
                    ),
                   
                    ],
                  ),
                ),
              ),
            ),

            //pad
            Expanded(
            flex: 2,
            // child: Container(
            //   color: Color.fromARGB(255, 114, 8, 96),
            // ),
            child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: GridView.builder(itemCount: NumPad.length,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(childAspectRatio: (100/20),
                crossAxisCount: 1), itemBuilder: (context,index){
                return NumButton(child: NumPad[index],onTap: () => buttonPressFunction(NumPad[index]),);
            }),
            ),
            ),
            
        ],
      ),
    ),
    );
  }
}

class NumButton extends StatelessWidget{
  final String child;
  final VoidCallback onTap;
  var btnColor = Color.fromARGB(255, 243, 187, 3);

  NumButton({Key? key,required this.child,required this.onTap,}) :super(key: key);

  @override
  Widget build(BuildContext context){
    if(child == 'C'){
      btnColor = Color.fromARGB(255, 219, 0, 0);
    }else if(child == 'OK'){
      btnColor = Color.fromARGB(255, 38, 210, 4);
    }


    return Padding(padding: const EdgeInsets.all(4.0),
                child: GestureDetector(
                  onTap: onTap,
                  child: Container(
                    decoration: BoxDecoration(
                      color: btnColor,
                      borderRadius: BorderRadius.circular(3),
                    ),
                    child: Center(
                      child: Text(child,style: GoogleFonts.bungee(
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                          fontSize: 30,
                          ),
                          ),
                    ),
                  ),
                ),
              );
  }
}

class AppLayout {
  static const double widthFirstBreakpoint = 450.0;
}

Widget titleWidget(BuildContext context, bool landscapeHeight) {
  return Padding(
      padding: const EdgeInsets.only(top: 26.0, bottom: 10.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios_new,
                  color: Colors.white,
                )),
          ),
          Expanded(
              flex: 5,
              child: Text(
                "Kids Math",
                style: GoogleFonts.bungee(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
                textScaleFactor: landscapeHeight ? 2.7 : 2.2,
                textAlign: TextAlign.center,
              )),
          const Expanded(flex: 1, child: Text(""))
        ],
      ));
}
